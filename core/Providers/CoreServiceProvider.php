<?php

namespace Core\Providers;

use Core\Repositories\Eloquent\BookRepository;
use Core\Repositories\Interfaces\BookInterface;
use Core\Services\BookService;
use Core\Services\BookServiceInterface;
use Illuminate\Support\ServiceProvider;

class CoreServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(BookInterface::class, BookRepository::class);
        $this->app->bind(BookServiceInterface::class, BookService::class);
    }
}