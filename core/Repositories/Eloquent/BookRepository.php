<?php 

namespace Core\Repositories\Eloquent;

use App\Book;
use Core\Repositories\Interfaces\BookInterface;

class BookRepository implements BookInterface
{
    protected $model;

    public function __construct(Book $model)
    {
        $this->model = $model;
    }

    public function paginate()
    {
        return $this->model->paginate(10);
    }

    public function find($id)
    {
        return $this->model->findOrFail($id);
    }

    public function store($data)
    {
        return $this->model->create($data);
    }

    public function update($id, $data) 
    {
        return $this->model->update(['id' => $id], $data);
    }

    public function destroy($id)
    {
        $model = $this->find($id);
        return $model->destroy($id);
    }
}